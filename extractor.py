#!/usr/bin/env python
# -*- coding:utf-8 -*-

import argparse
import os
from git import Repo
import inspect

def file_commit(repo, files):
    directory = os.path.dirname(inspect.getfile(inspect.currentframe()))
    output = open(os.path.join(directory, "file_commit.csv"), "w")
    for f in files:
        total = 1
        for _ in repo.head.commit.iter_parents(f):
            total += 1
        log = f + ',' + str(total)
        print (log)
        output.write(log)
    output.close()

def git_extractor(git_name):
    l = list()
    if not os.path.exists("pool/" + git_name):
        os.makedirs("pool/" + git_name)
    os.chdir(git_name)
    for root, dirs, files in os.walk("."):
        if '.git' in dirs:
            dirs.remove('.git')
        for f in files:
            l.append(os.path.join(root, f))
    repo = Repo(git_name)
    print 'git repo : ' + repo.remotes.origin.url

    file_commit(repo, l)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(\
            description='Extract logs from git repo to csv files for processing')
    parser.add_argument('git_destination',\
            help='Destination of the git repo')
    args = parser.parse_args()

    git_extractor(args.git_destination)
