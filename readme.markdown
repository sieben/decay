Decay project
==============

The goal of this project is to find correlation between the number of
commit of a file and the stability of a project.

The question of this project is to answer if it's possible after a
certain time to consider a file as statistically stable.
